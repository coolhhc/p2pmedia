module Bootstrap.DatabaseIO (
  runDBIO,
  getBootstrapConfig,
  clearBootstrapConfig,
  clearPeersLogs,
  clearMessages,
  clearQueryArticle
  ) where

import Import
import Settings
import Yesod.Default.Config
import Network.HTTP.Conduit (newManager, def)
import System.IO (stdout)
import System.Log.FastLogger (mkLogger)

import Control.Monad.Trans.Resource (runResourceT, ResourceT)
import Control.Monad.Logger (runLoggingT, LoggingT)
import System.Log.FastLogger (Logger)
import qualified Database.Persist.Store as PS (loadConfig, applyEnv, createPoolConfig, 
                                               runPool, PersistConfigPool, PersistConfigBackend)
import Database.Persist.Sqlite (SqliteConf)
import Data.Text as T (unpack)


-- ** db operations
getBootstrapConfig :: AppConfig DefaultEnv Extra -> IO (String, String, String)
getBootstrapConfig conf = do
  maybeBS <- runDBIO conf $ selectFirst ([] :: [Filter Bootstrap]) []
  case maybeBS of
    Just (Entity _ val) -> do
      let host  = T.unpack $ bootstrapIp   val
          port  = T.unpack $ bootstrapPort val
          seeds = T.unpack $ bootstrapSeed val
      return (host, port, seeds)
    Nothing -> error "getBootstrapConfig: No bootstrap configure found."


clearBootstrapConfig :: AppConfig DefaultEnv Extra -> IO ()
clearBootstrapConfig conf = do
  putStrLn $ "Clearing old bootstrap configurations...."
  runDBIO conf $ deleteWhere ([] :: [Filter Bootstrap])

clearPeersLogs :: AppConfig DefaultEnv Extra -> IO ()
clearPeersLogs conf = do  
  putStrLn $ "Clearing old peer logs...."
  runDBIO conf $ deleteWhere ([] :: [Filter Peer])

clearMessages :: AppConfig DefaultEnv Extra -> IO ()
clearMessages conf = do
  putStrLn $ "Clearing old peer messages...."
  runDBIO conf $ deleteWhere ([] :: [Filter Message])

clearQueryArticle :: AppConfig DefaultEnv Extra -> IO ()
clearQueryArticle conf = do
  putStrLn $ "Clearing old queried articles...."
  runDBIO conf $ deleteWhere ([] :: [Filter QueryArticle])

-- ** running db in IO
runDBIO :: AppConfig DefaultEnv Extra -> 
              PS.PersistConfigBackend Database.Persist.Sqlite.SqliteConf
                (Control.Monad.Logger.LoggingT (Control.Monad.Trans.Resource.ResourceT IO)) a -> 
              IO a
runDBIO conf queries = do 
  (dbconf, p, foundation, logger) <- dbSetup conf
  db <- runResourceT $ runLoggingT
         (PS.runPool dbconf queries p)
         (messageLoggerSource foundation logger)
  return db

dbSetup :: AppConfig DefaultEnv Extra -> IO ((PersistConfig, PS.PersistConfigPool PersistConfig, 
                                              App, System.Log.FastLogger.Logger))
dbSetup conf = do
  manager <- newManager def 
  s <- staticSite
  dbconf <- withYamlEnvironment "config/sqlite.yml" (appEnv conf)
            PS.loadConfig >>= 
            PS.applyEnv
  p <- PS.createPoolConfig (dbconf :: Settings.PersistConfig)
  logger <- mkLogger True stdout
  let foundation = App conf s p manager dbconf logger
  return (dbconf, p, foundation, logger)
