module Handler.Messages where
import Import
import Database.Persist.Store
import Handler.HandlerNode.NodeActions (handlerNode, getP2PNode)
import qualified P2PModule.Internal.Impl as P2P (Action(..))
import Data.Text (unpack)
import Control.Concurrent (threadDelay)

getMessagesR :: Handler RepHtml
getMessagesR = do
    (formWidget, formEnctype) <- generateFormPost msgForm
    msgs <- runDB $ selectList ([] :: [Filter Message]) []
    defaultLayout $ do
        setTitle "Message"
        $(widgetFile "header")
        $(widgetFile "messages")

postMessagesR :: Handler RepHtml
postMessagesR = do
  ((result, _), _) <- runFormPost msgForm
  let formData = case result of
        FormSuccess res ->  Just res 
        _               ->  Nothing
  _ <- case formData of
         Just msgF -> do
           let msg = unpack $ unTextarea $ message msgF
           p2pNode <- getP2PNode
           case p2pNode of
           --TODO fix magic number
             Just (host, port, hport, _) -> handlerNode (host, hport, host ++ ":" ++ port) (P2P.SendMessage msg)
             Nothing -> notFound
         Nothing -> notFound
  liftIO $ threadDelay (3 * 1000000)
  redirect MessagesR

data MsgForm = MsgForm {
  message :: Textarea
  }
  deriving (Show, Eq) 

msgForm :: Html -> MForm App App (FormResult MsgForm, Widget)
msgForm = renderDivs $ MsgForm
  <$> areq textareaField "Post to feed:" Nothing
