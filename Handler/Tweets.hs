module Handler.Tweets where

import Import

getTweetsR :: Handler RepHtml
getTweetsR = do
    defaultLayout $ do
        aDomId <- lift newIdent
        setTitle "Welcome To Yesod!"
        $(widgetFile "tweets")
