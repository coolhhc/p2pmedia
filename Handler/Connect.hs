{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Connect where

import Import

getConnectR :: Handler RepHtml
getConnectR = do
    (formWidget, formEnctype) <- generateFormPost connectForm
    defaultLayout $ do
        setTitle "Connection to peer pool!"
        addStylesheet $ StaticR css_bootstrap_css
        $(widgetFile "header")
        $(widgetFile "connect")


postConnectR :: Handler RepHtml
postConnectR = do
  ((result, _), _) <- runFormPost connectForm
  let formData = case result of
        FormSuccess res ->  Just res 
        _               ->  Nothing
  _ <- case formData of  
         Just connect -> runDB $ insert $ Bootstrap (host connect) (port connect) (hport connect) (seeds connect) 
         Nothing      -> notFound
  
  redirect $ HomeR

data ConnectForm = ConnectForm {
  host :: Text,
  port :: Text,
  hport :: Text,
  seeds :: Text
  }
  deriving (Show, Eq) 

connectForm :: Html -> MForm App App (FormResult ConnectForm, Widget) 
connectForm = renderDivs $ ConnectForm
  <$> areq textField "Localhost IP" Nothing
  <*> areq textField "Port for P2PModule in this node" Nothing
  <*> areq textField "Port for Handler in this node" Nothing
  <*> areq textField "Seeds (etc. 127.0.0.1:9001):" Nothing
