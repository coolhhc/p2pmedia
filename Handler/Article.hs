module Handler.Article where

import Import
import Text.Blaze.Internal (preEscapedText)
import Data.Text (unpack)
import Data.Time (getCurrentTime)
import Control.Concurrent (threadDelay)
import Database.Persist.Store

import Handler.HandlerNode.NodeActions (handlerNode, getP2PNode)
import qualified P2PModule.Internal.Impl as P2P (Action(..))

getArticleR :: Handler RepHtml
getArticleR = do
  queryArticles <- runDB $ selectList ([] :: [Filter QueryArticle]) []
  defaultLayout $ do
      setTitle "Search For Article"
      $(widgetFile "header")
      $(widgetFile "article")

getArticleDisplayR :: QueryArticleId -> Handler RepHtml
getArticleDisplayR articleId =  do 
  article <- runDB $ get404 articleId
  let titleA   = queryArticleTitle article
      contextA = preEscapedText $ queryArticleContext article
  defaultLayout $ do
      setTitle "Article"
      $(widgetFile "header")
      $(widgetFile "article-display")

postArticleR :: Handler RepHtml
postArticleR = do
  _ <- runDB $ deleteWhere ([] :: [Filter QueryArticle])
  searchWrapper <- runInputPost $ Search <$> ireq textField "search"
  let query = unpack $ search searchWrapper
  p2pNode <- getP2PNode
  _ <- case p2pNode of
         Just (host, port, hport, _) -> handlerNode (host, hport, host ++ ":" ++ port) (P2P.QueryArticle query)
         Nothing -> notFound
  liftIO $ threadDelay (3 * 1000000)
  redirect ArticleR

getArticleLocalR :: Handler RepHtml
getArticleLocalR = do
  articles <- runDB $ selectList ([] :: [Filter Article]) [Desc ArticleTime]
  defaultLayout $ do
      setTitle "Local Article"
      $(widgetFile "header")
      $(widgetFile "article-local")

getArticleLocalDisplayR :: ArticleId -> Handler RepHtml
getArticleLocalDisplayR articleId =  do 
  article <- runDB $ get404 articleId
  let titleA   = articleTitle article
      contextA = preEscapedText $ articleContext article
  defaultLayout $ do
      setTitle "Article"
      $(widgetFile "header")
      $(widgetFile "article-display")

getArticleLocalCreateR :: Handler RepHtml
getArticleLocalCreateR = do
  defaultLayout $ do
      setTitle "Create article"
      addScript $ StaticR ckeditor_ckeditor_js
      $(widgetFile "header")
      $(widgetFile "article-local-create")

postArticleLocalCreateR :: Handler RepHtml
postArticleLocalCreateR = do
  articleForm <- runInputPost $ ArticleForm <$> ireq textField "title" <*> ireq textareaField "editor1"
  now         <- liftIO getCurrentTime
  let titleA  =  title articleForm
      html    =  unTextarea $ context articleForm
  _           <- runDB $ insert $ Article titleA html now
  redirect ArticleLocalR

data ArticleForm = ArticleForm {
    title   :: Text,
    context :: Textarea
  }
  deriving Show

data Search = Search { search :: Text }
  deriving Show
