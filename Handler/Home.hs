{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Home where

import Import

-- NEED TO REDO HOMER
getHomeR :: Handler RepHtml
getHomeR = do
    defaultLayout $ do
        aDomId <- lift newIdent
        setTitle "Welcome To Yesod!"
        $(widgetFile "header")
        $(widgetFile "homepage")
