module Handler.HandlerNode.NodeActions (
  handlerNode,
  getP2PNode
  ) where

import Import
import Network.Transport.TCP (createTransport, defaultTCPParameters)
import Network.Transport (closeTransport, Transport)
import Control.Distributed.Process (NodeId, ProcessId, Process, receiveWait, matchIf, say, getSelfPid, newChan, 
                                    send, receiveChan, SendPort, WhereIsReply(..))
import Control.Distributed.Process.Node (newLocalNode, initRemoteTable, closeLocalNode, runProcess)
import Control.Concurrent.MVar (newEmptyMVar, takeMVar, putMVar, MVar)
import qualified Data.Set as S (toList)
import           Data.Text as T (unpack)
import qualified P2PModule.Internal.Impl as P2P (BinaryArticle(..), Action(..), HandlerMsg(..),
                                                             makeNodeId, Peers, isPeerDiscover, doDiscover)

getP2PNode :: Handler (Maybe (String, String, String, String))
getP2PNode = do
  maybeBS <- runDB $ selectFirst ([] :: [Filter Bootstrap]) []
  case maybeBS of
    Just (Entity _ val) -> do
      let host  = T.unpack $ bootstrapIp    val
          port  = T.unpack $ bootstrapPort  val
          hport = T.unpack $ bootstrapHport val
          seeds = T.unpack $ bootstrapSeed  val
      return $ Just (host, port, hport, seeds)
    Nothing -> return Nothing

handlerNode :: (String, String, String) -> P2P.Action -> Handler String
handlerNode (host, port, seedNode) action = do
  --transport <- either (error . show) id `fmap` createTransport host port defaultTCPParameters
  Right transport <- liftIO $ createTransport host port defaultTCPParameters
  localNode <- liftIO $ newLocalNode transport initRemoteTable  
  seedMVar  <- liftIO $ newEmptyMVar
  _ <- liftIO $ runProcess localNode $ getP2PSeedPid (P2P.makeNodeId seedNode) seedMVar

  mvar <- case action of
         P2P.GetPeers -> do
           peersMVar <- liftIO $ newEmptyMVar
           liftIO $ runProcess localNode $ getP2PPeers seedMVar peersMVar action
           peers <- liftIO $ takeMVar peersMVar
           liftIO $ putStrLn $ "Handler acquired peers from p2pModule: " ++ show peers
           return $ show peers
         P2P.SendMessage _ -> do
           isSentMVar <- liftIO $ newEmptyMVar
           liftIO $ runProcess localNode $ sendMsg seedMVar isSentMVar action
           isSent <- liftIO $ takeMVar isSentMVar
           liftIO $ putStrLn $ "Handler successfully sent message"
           return $ show isSent
         P2P.QueryArticle _ -> do
           articlesMVar <- liftIO $ newEmptyMVar
           liftIO $ runProcess localNode $ sendQuery seedMVar articlesMVar action
           articles <- liftIO $ takeMVar articlesMVar
           saveQueryArticle articles
           liftIO $ putStrLn $ "Handler acquired queried articles"
           return $ "Query stored"
         _ -> error "action not found"

  liftIO $ closeLocalNode localNode
  liftIO $ closeTransport transport
  return mvar

saveQueryArticle :: [P2P.BinaryArticle] -> Handler ()
saveQueryArticle article = do
  liftIO $ putStrLn $ "Storing queried results...."
  mapM_ (runDB . insert . binary2QueryArticle) article
  return ()

binary2QueryArticle :: P2P.BinaryArticle -> QueryArticle
binary2QueryArticle ba = QueryArticle (P2P.title ba) (P2P.context ba) (P2P.time ba)

getP2PSeedPid :: NodeId -> MVar ProcessId -> Process ()
getP2PSeedPid seedNodeId seedMVar = do  
  P2P.doDiscover seedNodeId
  seedPid <- receiveWait [ matchIf P2P.isPeerDiscover $ (\(WhereIsReply _ (Just seedPid)) -> return seedPid) ]
  liftIO $ putMVar seedMVar seedPid
  return ()

getP2PPeers :: MVar ProcessId -> MVar [ProcessId] -> P2P.Action -> Process ()
getP2PPeers seedMVar peersMVar action = do
  self     <- getSelfPid
  seedPid  <- liftIO $ takeMVar seedMVar
  (sp, rp) <- newChan
  send seedPid (P2P.HandlerMsg self action, sp :: SendPort P2P.Peers)
  peers    <- receiveChan rp
  liftIO $ putMVar peersMVar $ S.toList peers
  return ()

sendMsg :: MVar ProcessId -> MVar Bool -> P2P.Action -> Process ()
sendMsg seedMVar isSentMVar action = do
  self     <- getSelfPid
  seedPid  <- liftIO $ takeMVar seedMVar
  (sp, rp) <- newChan
  send seedPid (P2P.HandlerMsg self action , sp :: SendPort Bool)
  isSent   <- receiveChan rp
  liftIO $ putMVar isSentMVar isSent
  return ()

-- TODO should return url so that we can link to article that will be served via target machine handler
sendQuery :: MVar ProcessId -> MVar [P2P.BinaryArticle] -> P2P.Action -> Process ()
sendQuery seedMVar articlesMVar action = do
  self     <- getSelfPid
  seedPid  <- liftIO $ takeMVar seedMVar
  (sp, rp) <- newChan
  send seedPid (P2P.HandlerMsg self action , sp :: SendPort [P2P.BinaryArticle])
  articles <- receiveChan rp
  liftIO $ putMVar articlesMVar articles
  return ()
