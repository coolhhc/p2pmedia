module P2PModule.Listener (
  listenerProcess
  ) where
import Import
import qualified P2PModule.Internal.Impl as P2P 
import           Control.Distributed.Process as DP
import Control.Monad
import Control.Concurrent (threadDelay)
import Bootstrap.DatabaseIO (runDBIO) 
import Yesod.Default.Config
import Data.Text as T (pack)

import Database.Persist.Store


listenerProcess :: AppConfig DefaultEnv Extra -> Process ()
listenerProcess conf = do
  _ <- spawnLocal p2pLogger
  _ <- spawnLocal $ listenMsg conf
  _ <- spawnLocal $ articleSearcher conf
  return ()

articleSearcher :: AppConfig DefaultEnv Extra -> Process ()
articleSearcher conf = do
  getSelfPid >>= register "queryService"
  forever $ do
    (action, replayTo) <- expect :: Process (P2P.Action, SendPort [P2P.BinaryArticle])
    {- TODO only mach exact query, need to change to fuzzy match -}
    {-
    let icontains field val = Filter field (Left $ PersistText $ concat ["%", val,"%"]) (BackendSpecificFilter"ILIKE")
    articles <- liftIO $ runDBIO conf $ selectList [ArticleTitle `icontains` ((T.pack.P2P.getQuery) action)] []
    -}
    articles <- liftIO $ runDBIO conf $ selectList [ArticleTitle <-. [(T.pack.P2P.getQuery) action]] []
    sendChan replayTo $ map (\(Entity _ (Article title context time)) -> P2P.BinaryArticle title context time) articles
    return ()

listenMsg ::  AppConfig DefaultEnv Extra -> Process ()
listenMsg conf = do
  getSelfPid >>= register "messageService"
  forever $ do
    (pid, msg) <- expect :: Process (ProcessId, String)
    liftIO $ putStrLn $ "Message sent from: " ++ show pid ++ " Content: " ++ show msg
    _ <- liftIO $ runDBIO conf $ insert $ Message (T.pack $ show pid) (T.pack $ msg)
    return ()

p2pLogger :: Process ()
p2pLogger = do
    unregister "logger"
    getSelfPid >>= register "logger"
    forever $ do
        (time, pid, msg) <- expect :: Process (String, ProcessId, String)
        liftIO $ putStrLn $ time ++ " " ++ show pid ++ " " ++ msg 
        return ()
