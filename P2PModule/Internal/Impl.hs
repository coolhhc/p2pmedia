{-# LANGUAGE OverloadedStrings, RecordWildCards, DeriveDataTypeable #-}
module P2PModule.Internal.Impl (
    bootstrap,
    makeNodeId,
    getPeers,
    getCapable,
    nsendPeers,
    nsendCapable,
    BinaryArticle(..),
    Action(..),
    HandlerMsg(..),
    Peers,
    doDiscover,
    isPeerDiscover
  ) where

import Import
import P2PModule.Internal.API (makeNodeId, getPeers, getCapable, nsendPeers, nsendCapable)
import P2PModule.Internal.Type (peerControllerService, Peers, PeerState (..))
import P2PModule.Internal.Handler (BinaryArticle(..), Action(..), HandlerMsg(..))

import Control.Distributed.Process                as DP
import Control.Distributed.Process.Node           as DPN 

import Network.Transport.TCP (createTransport, defaultTCPParameters)

import Control.Monad
import Control.Concurrent.MVar
import qualified Data.Set as S
import Data.Maybe (isJust)

import Data.Time (getCurrentTime)
import Data.Text (pack)

-- | Start a controller service process and aquire connections to a swarm.
bootstrap :: String -> String -> [NodeId] -> Process () -> IO ()
bootstrap host port seeds proc = do
    transport <- either (error . show) id `fmap` createTransport host port defaultTCPParameters
    node <- newLocalNode transport initRemoteTable

    _ <- forkProcess node $ do
        state <- initPeerState
        mapM_ doDiscover seeds
        say "P2P controller started."
        forever $ receiveWait [ matchIf isPeerDiscover $ onDiscover state
                              , match $ onMonitor state
                              , match $ onPeerRequest state
                              , match $ onPeerQuery state
                              , match $ onPeerCapable
                              , match $ onHandlerPeerRequest state
                              , match $ onHandlerMessageSend state
                              , match $ onHandlerQueryArticle state
                              ]
         
    runProcess node proc

---------------------------------------------------------------------------------
-- Article Query
---------------------------------------------------------------------------------
onHandlerQueryArticle :: PeerState -> (HandlerMsg, SendPort [BinaryArticle]) -> Process ()
onHandlerQueryArticle PeerState{..} (handlerMsg, replyTo) = do
  peers <- liftIO $ takeMVar p2pPeers
  pid   <- getSelfPid
  queryArticles <- sendQuerys "queryService" peers (handlerAction handlerMsg)
  sendChan replyTo queryArticles
  say $ "P2PModule reply: onHandlerQueryArticle...."
  liftIO $ putMVar p2pPeers peers  

sendQuerys :: String -> Peers -> Action -> Process ([BinaryArticle])
sendQuerys service peers action = do
  let nodeIds = map processNodeId (S.toList peers)
  lists <- mapM (\nodeId -> sendOneQuery "queryService" nodeId action) nodeIds
  return $ concat lists

sendOneQuery :: String -> NodeId -> Action -> Process ([BinaryArticle])
sendOneQuery service nodeId action = do
  (sp, rp) <- newChan
  nsendRemote nodeId service (action, sp :: SendPort [BinaryArticle])
  binaryArticle <- receiveChan rp
  return binaryArticle

---------------------------------------------------------------------------------
-- Send Global Message
---------------------------------------------------------------------------------
onHandlerMessageSend :: PeerState -> (HandlerMsg, SendPort Bool) -> Process ()
onHandlerMessageSend PeerState{..} (handlerMsg, replyTo) = do
  let action = handlerAction handlerMsg
      msg    = getMsg action
  peers <- liftIO $ takeMVar p2pPeers
  pid   <- getSelfPid
  sendMsg "messageService" peers (pid, msg)
  sendChan replyTo True
  say $ "P2PModule reply: onHandlerMessageSend...."
  liftIO $ putMVar p2pPeers peers  

sendMsg :: String -> Peers -> (ProcessId, String) -> Process ()
sendMsg service peers msg = do
  let nodeIds = map processNodeId (S.toList peers)
  mapM_ (\nodeId -> nsendRemote nodeId service msg) nodeIds
  return ()

---------------------------------------------------------------------------------
-- Return Peer to Handler
---------------------------------------------------------------------------------
onHandlerPeerRequest :: PeerState -> (HandlerMsg, SendPort Peers) -> Process ()
onHandlerPeerRequest PeerState{..} (_, replyTo) = do
  peers <- liftIO $ takeMVar p2pPeers
  liftIO $ putMVar p2pPeers peers
  sendChan replyTo peers
  say $ "P2PModule reply: onHandlerPeerRequest...."

---------------------------------------------------------------------------------
-- P2P module
---------------------------------------------------------------------------------

-- * Peer-to-peer API
initPeerState :: Process PeerState
initPeerState = do
    self <- getSelfPid
    peers <- liftIO $ newMVar (S.singleton self)
    register peerControllerService self
    return $! PeerState peers

-- ** Discovery
doDiscover :: NodeId -> Process ()
doDiscover node = do
    say $ "Examining node: " ++ show node
    whereisRemoteAsync node peerControllerService

isPeerDiscover :: WhereIsReply -> Bool
isPeerDiscover (WhereIsReply service pid) =
    service == peerControllerService && isJust pid

onDiscover :: PeerState -> WhereIsReply -> Process ()
onDiscover state (WhereIsReply _ (Just seedPid)) = do
    say $ "Peer discovered: " ++ show seedPid

    (sp, rp) <- newChan
    self <- getSelfPid
    send seedPid (self, sp :: SendPort Peers)
    say $ "Waiting for peers..."
    peers <- receiveChan rp

    known <- liftIO $ readMVar (p2pPeers state)
    mapM_ (doRegister state) (S.toList $ S.difference peers known) -- register and look at exchanged peers list
onDiscover _ _ = error "error: onDiscover"

doRegister :: PeerState -> ProcessId -> Process ()
doRegister PeerState{..} pid = do
    pids <- liftIO $ takeMVar p2pPeers
    if S.member pid pids
        then liftIO $ putMVar p2pPeers pids -- pid (Node) already in the list
        else do
            say $ "Registering peer:" ++ show pid
            _ <- monitor pid

            liftIO $ putMVar p2pPeers (S.insert pid pids)
            say $ "New node: " ++ show pid
            doDiscover $ processNodeId pid

doUnregister :: PeerState -> Maybe MonitorRef -> ProcessId -> Process ()
doUnregister PeerState{..} mref pid = do
    say $ "Unregistering peer: " ++ show pid
    maybe (return ()) unmonitor mref
    peers <- liftIO $ takeMVar p2pPeers
    liftIO $ putMVar p2pPeers (S.delete pid peers)

onPeerRequest :: PeerState -> (ProcessId, SendPort Peers) -> Process ()
onPeerRequest PeerState{..} (peer, replyTo) = do
    say $ "Peer exchange with " ++ show peer
    peers <- liftIO $ takeMVar p2pPeers
    if S.member peer peers
        then liftIO $ putMVar p2pPeers peers
        else do
            _ <- monitor peer
            liftIO $ putMVar p2pPeers (S.insert peer peers)

    sendChan replyTo peers

onPeerQuery :: PeerState -> SendPort Peers -> Process ()
onPeerQuery PeerState{..} replyTo = do
    say $ "Local peer query."
    liftIO (readMVar p2pPeers) >>= sendChan replyTo

onPeerCapable :: (String, SendPort ProcessId) -> Process ()
onPeerCapable (service, replyTo) = do
    say $ "Capability request: " ++ service
    res <- whereis service
    case res of
        Nothing -> say "I can't."
        Just pid -> say "I can!" >> sendChan replyTo pid

onMonitor :: PeerState -> ProcessMonitorNotification -> Process ()
onMonitor state (ProcessMonitorNotification mref pid reason) = do
    say $ "Monitor event: " ++ show (pid, reason)
    _ <- doUnregister state (Just mref) pid
    return ()
